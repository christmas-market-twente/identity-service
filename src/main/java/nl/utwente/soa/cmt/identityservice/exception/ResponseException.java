package nl.utwente.soa.cmt.identityservice.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public abstract class ResponseException extends Exception {

    private HttpStatus status;

    private String message;

    public ResponseException(HttpStatus status, String message) {
        this.status = status;
        this.message = message;
    }

    public ResponseException(HttpStatus status) {
        this(status, null);
    }

    public ResponseException() {
        this(HttpStatus.INTERNAL_SERVER_ERROR);
    }
}