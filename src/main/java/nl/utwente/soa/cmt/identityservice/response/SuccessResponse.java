package nl.utwente.soa.cmt.identityservice.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SuccessResponse<T> extends Response {

    private T data;

    public SuccessResponse(T data) {
        super(true);
        this.data = data;
    }
}
