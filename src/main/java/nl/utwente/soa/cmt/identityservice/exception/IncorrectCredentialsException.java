package nl.utwente.soa.cmt.identityservice.exception;

import org.springframework.http.HttpStatus;

public class IncorrectCredentialsException extends ResponseException {

    public IncorrectCredentialsException() {
        super(HttpStatus.UNAUTHORIZED, "Incorrect email or password");
    }
}
