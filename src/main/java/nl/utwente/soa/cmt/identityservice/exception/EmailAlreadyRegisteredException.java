package nl.utwente.soa.cmt.identityservice.exception;

import org.springframework.http.HttpStatus;

public class EmailAlreadyRegisteredException extends ResponseException {

    public EmailAlreadyRegisteredException() {
        super(HttpStatus.CONFLICT, "The email address is already registered");
    }
}
