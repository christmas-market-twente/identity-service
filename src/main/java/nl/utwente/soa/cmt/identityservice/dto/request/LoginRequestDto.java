package nl.utwente.soa.cmt.identityservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginRequestDto {

    @NotNull
    private String email;

    @NotNull
    private String password;
}
