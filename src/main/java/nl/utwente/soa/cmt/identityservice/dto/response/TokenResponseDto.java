package nl.utwente.soa.cmt.identityservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TokenResponseDto {

    public TokenResponseDto(String token) {
        this.token = token;
    }

    public TokenResponseDto() {}

    private String token;
}
