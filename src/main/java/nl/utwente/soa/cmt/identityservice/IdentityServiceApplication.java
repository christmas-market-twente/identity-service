package nl.utwente.soa.cmt.identityservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EnableScheduling
public class IdentityServiceApplication {

    @Value("${docs.service.name}")
    private String serviceName;

    @Value("${docs.service.url}")
    private String serviceUrl;

    @Value("${docs.subscription-url}")
    private String docSubscriptionUrl;

    private static final Logger logger = LoggerFactory.getLogger(IdentityServiceApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(IdentityServiceApplication.class, args);
    }

    @Scheduled(fixedRate = 10 * 60 * 1000)
    public void subscribeToDocumentationService() {
        RestTemplate restTemplate = new RestTemplate();
        Map<String, String> body = new HashMap<>();
        body.put("name", serviceName);
        body.put("url", serviceUrl);

        HttpEntity<Map<String, String>> request = new HttpEntity<>(body);
        try {
            restTemplate.postForEntity(docSubscriptionUrl, request, Void.class);
        } catch (RestClientException e) {
            logger.error("Cannot connect to the documentation service", e);
        }
    }
}
