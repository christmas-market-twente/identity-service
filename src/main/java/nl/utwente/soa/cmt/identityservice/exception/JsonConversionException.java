package nl.utwente.soa.cmt.identityservice.exception;

import org.springframework.http.HttpStatus;

public class JsonConversionException extends ResponseException {

    public JsonConversionException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, "Cannot convert from or to JSON");
    }
}
