package nl.utwente.soa.cmt.identityservice.exception;

import org.springframework.http.HttpStatus;

public class InvalidTokenException extends ResponseException {

    public InvalidTokenException() {
        super(HttpStatus.UNAUTHORIZED, "Invalid token");
    }
}
