package nl.utwente.soa.cmt.identityservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;
import nl.utwente.soa.cmt.identityservice.model.User;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserResponseDto {

    private String uuid;

    private String firstName;

    private String lastName;

    private String email;

    public static UserResponseDto from(User user) {
        UserResponseDto responseDto = new UserResponseDto();
        responseDto.setUuid(user.getUuid());
        responseDto.setFirstName(user.getFirstName());
        responseDto.setLastName(user.getLastName());
        responseDto.setEmail(user.getEmail());
        return responseDto;
    }
}
