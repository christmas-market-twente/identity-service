package nl.utwente.soa.cmt.identityservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.identityservice.dto.request.LoginRequestDto;
import nl.utwente.soa.cmt.identityservice.dto.request.RegisterRequestDto;
import nl.utwente.soa.cmt.identityservice.dto.response.TokenResponseDto;
import nl.utwente.soa.cmt.identityservice.dto.response.UserResponseDto;
import nl.utwente.soa.cmt.identityservice.exception.EmailAlreadyRegisteredException;
import nl.utwente.soa.cmt.identityservice.exception.IncorrectCredentialsException;
import nl.utwente.soa.cmt.identityservice.exception.InvalidTokenException;
import nl.utwente.soa.cmt.identityservice.exception.JsonConversionException;
import nl.utwente.soa.cmt.identityservice.response.ErrorResponse;
import nl.utwente.soa.cmt.identityservice.service.AuthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

@RestController
@RequestMapping("/auth")
@Tag(name = "Authentication", description = "The Authentication API")
public class AuthController {

    @Autowired
    private AuthService authService;

    @Operation(summary = "Register as a user", description = "Creates a new user account and returns a valid JWTtoken", tags = {"Authentication"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "409", description = "Email already registered",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("/register")
    public TokenResponseDto register(@Parameter(description = "Name, email and password of the user.")
                                        @Valid @RequestBody RegisterRequestDto registerRequestDto) throws EmailAlreadyRegisteredException {
        return authService.register(registerRequestDto);
    }

    @Operation(summary = "Login as a user", description = "Creates a new user account and returns a valid JWTtoken", tags = {"Authentication"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class))),
        @ApiResponse(responseCode = "401", description = "Incorrect credentials",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("/login")
    public TokenResponseDto login(@Parameter(description = "Email and password of the user account.")
                                    @Valid @RequestBody LoginRequestDto loginRequestDto) throws IncorrectCredentialsException {
        return authService.login(loginRequestDto);
    }

    @Operation(summary = "Validate JWT token", description = "Validates the token and returns the user data in a header.", tags = {"Authentication"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "401", description = "Invalid X-Authorization header",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @Parameter(in = ParameterIn.HEADER, name = "X-Authorization", description = "Authorization header in the form 'Bearer <token>'")
    @PostMapping("/validate")
    public void validate(HttpServletRequest request, HttpServletResponse response) throws InvalidTokenException, JsonConversionException {
        String authHeader = request.getHeader("X-Authorization");
        UserResponseDto user = authService.validate(authHeader);

        ObjectMapper mapper = new ObjectMapper();
        String userJson;
        try {
            userJson = mapper.writeValueAsString(user);
        } catch (JsonProcessingException e) {
            throw new JsonConversionException();
        }

        response.setHeader("X-User", Base64.getEncoder().encodeToString(userJson.getBytes(StandardCharsets.UTF_8)));
        response.setHeader("X-User-UUID", user.getUuid());
    }
}
