package nl.utwente.soa.cmt.identityservice.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse extends Response {

    private String message;

    public ErrorResponse(String message) {
        super(false);
        this.message = message;
    }
}
