package nl.utwente.soa.cmt.identityservice.service;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import nl.utwente.soa.cmt.identityservice.dto.request.LoginRequestDto;
import nl.utwente.soa.cmt.identityservice.dto.request.RegisterRequestDto;
import nl.utwente.soa.cmt.identityservice.dto.response.TokenResponseDto;
import nl.utwente.soa.cmt.identityservice.dto.response.UserResponseDto;
import nl.utwente.soa.cmt.identityservice.exception.EmailAlreadyRegisteredException;
import nl.utwente.soa.cmt.identityservice.exception.IncorrectCredentialsException;
import nl.utwente.soa.cmt.identityservice.exception.InvalidTokenException;
import nl.utwente.soa.cmt.identityservice.model.User;
import nl.utwente.soa.cmt.identityservice.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    @Value("${jwt.secret}")
    private String jwtSecret;

    private PasswordEncoder passwordEncoder = new Argon2PasswordEncoder();

    public TokenResponseDto register(RegisterRequestDto registerRequestDto) throws EmailAlreadyRegisteredException {
        if (userRepository.existsByEmail(registerRequestDto.getEmail())) {
            throw new EmailAlreadyRegisteredException();
        }

        User user = new User();
        user.setUuid(UUID.randomUUID().toString());
        user.setFirstName(registerRequestDto.getFirstName());
        user.setLastName(registerRequestDto.getLastName());
        user.setEmail(registerRequestDto.getEmail());
        user.setPassword(passwordEncoder.encode(registerRequestDto.getPassword()));
        user.setRegisteredAt(new Date().getTime() / 1000);
        user = userRepository.save(user);

        return new TokenResponseDto(
            getTokenFor(user)
        );
    }

    public TokenResponseDto login(LoginRequestDto loginRequestDto) throws IncorrectCredentialsException {
        User user = userRepository.findByEmail(loginRequestDto.getEmail())
            .orElseThrow(IncorrectCredentialsException::new);

        if (!passwordEncoder.matches(loginRequestDto.getPassword(), user.getPassword())) {
            throw new IncorrectCredentialsException();
        }

        return new TokenResponseDto(
            getTokenFor(user)
        );
    }

    public UserResponseDto validate(String authHeader) throws InvalidTokenException {
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            throw new InvalidTokenException();
        }

        String token = authHeader.substring("Bearer ".length());
        Jws<Claims> jws;
        try {
            jws = Jwts.parserBuilder()
                .setSigningKey(Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8)))
                .build()
                .parseClaimsJws(token);
        } catch (JwtException e) {
            throw new InvalidTokenException();
        }

        String uuid = jws.getBody().getSubject();
        User user = userRepository.findByUuid(uuid)
            .orElseThrow(InvalidTokenException::new); // This should only happen if the user was logged in, but its account has been deleted.

        return UserResponseDto.from(user);
    }

    private String getTokenFor(User user) {
        Calendar expiration = Calendar.getInstance();
        expiration.add(Calendar.MINUTE, 30);

        return Jwts.builder()
            .setHeaderParam("typ", "JWT")
            .setIssuer("Christmas Market Twente")
            .setSubject(user.getUuid())
            .setExpiration(expiration.getTime())
            .setIssuedAt(new Date())
            .signWith(Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8)), SignatureAlgorithm.HS256)
            .compact();
    }
}
