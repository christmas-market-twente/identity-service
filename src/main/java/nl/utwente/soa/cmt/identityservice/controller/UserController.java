package nl.utwente.soa.cmt.identityservice.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.identityservice.dto.response.UserResponseDto;
import nl.utwente.soa.cmt.identityservice.exception.JsonConversionException;
import nl.utwente.soa.cmt.identityservice.response.ErrorResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

@RestController
@Tag(name = "User", description = "The User API")
public class UserController {

    @Operation(summary = "Get user information.", description = "Returns the logged in user.", tags = {"Authentication"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation")})
    @GetMapping("/me")
    public UserResponseDto me(@Parameter(hidden = true) @RequestHeader("X-User") String userHeader) throws JsonConversionException {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(
                new String(Base64.getDecoder().decode(userHeader), StandardCharsets.UTF_8),
                UserResponseDto.class
            );
        } catch (JsonProcessingException e) {
            throw new JsonConversionException();
        }
    }
}
